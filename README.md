Create a Student Registration Application, Where student had to register in 2 steps, 

Step 1: student select Year and Department. (Please find StudentRegistrationFirstImage.Png)
Step 2: student fill their personal information such as Profile Image, Name, Address(Permanent and Current), Phone Number(Personal and Guardian), Blood group, etc. (StudentRegistrationSecondImage.Png)

After successful registration Student goes to a page where there is a drawer menu having two menus. 
Menu 1: 

Name of the menu (Subject List)
The Student can see videos of the subjects they have. (Please find SubjectsVideo.Png)

Menu 2:
Name of the menu (Exam Location)
The student can see the list of locations in list view (Please find ExamLocation.Png)
After clicking on each list item it will open a map view where the location will be shown using a marker and clicking on an icon will open the Google Map to show the Navigation. (Please find ExamDetails.Png)


Things that you need to use to accomplish this:

**In order to do this you have to use 
1. ConstraintLayout for designing.
2. Use volley or Retrofit for API call.
3. YouTube API key: AIzaSyDGKARjlOIbRy-eaQ9WlZdvgJIKK_X7PYY
4. Google Maps API key: AIzaSyDGKARjlOIbRy-eaQ9WlZdvgJIKK_X7PYY